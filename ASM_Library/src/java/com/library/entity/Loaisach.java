/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Setup
 */
@Entity
@Table(name = "LOAISACH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Loaisach.findAll", query = "SELECT l FROM Loaisach l")
    , @NamedQuery(name = "Loaisach.findByMaloai", query = "SELECT l FROM Loaisach l WHERE l.maloai = :maloai")
    , @NamedQuery(name = "Loaisach.findByTenloai", query = "SELECT l FROM Loaisach l WHERE l.tenloai = :tenloai")})
public class Loaisach implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MALOAI")
    private Integer maloai;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TENLOAI")
    private String tenloai;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "maloai", fetch = FetchType.LAZY)
    private List<Sach> sachList;

    public Loaisach() {
    }

    public Loaisach(Integer maloai) {
        this.maloai = maloai;
    }

    public Loaisach(Integer maloai, String tenloai) {
        this.maloai = maloai;
        this.tenloai = tenloai;
    }

    public Integer getMaloai() {
        return maloai;
    }

    public void setMaloai(Integer maloai) {
        this.maloai = maloai;
    }

    public String getTenloai() {
        return tenloai;
    }

    public void setTenloai(String tenloai) {
        this.tenloai = tenloai;
    }

    @XmlTransient
    public List<Sach> getSachList() {
        return sachList;
    }

    public void setSachList(List<Sach> sachList) {
        this.sachList = sachList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maloai != null ? maloai.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loaisach)) {
            return false;
        }
        Loaisach other = (Loaisach) object;
        if ((this.maloai == null && other.maloai != null) || (this.maloai != null && !this.maloai.equals(other.maloai))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.library.entity.Loaisach[ maloai=" + maloai + " ]";
    }
    
}
