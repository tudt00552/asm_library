/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Setup
 */
@Entity
@Table(name = "TACGIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tacgia.findAll", query = "SELECT t FROM Tacgia t")
    , @NamedQuery(name = "Tacgia.findByMatg", query = "SELECT t FROM Tacgia t WHERE t.matg = :matg")
    , @NamedQuery(name = "Tacgia.findByTentacgia", query = "SELECT t FROM Tacgia t WHERE t.tentacgia = :tentacgia")
    , @NamedQuery(name = "Tacgia.findByDiachi", query = "SELECT t FROM Tacgia t WHERE t.diachi = :diachi")
    , @NamedQuery(name = "Tacgia.findBySdt", query = "SELECT t FROM Tacgia t WHERE t.sdt = :sdt")
    , @NamedQuery(name = "Tacgia.findByEmail", query = "SELECT t FROM Tacgia t WHERE t.email = :email")})
public class Tacgia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MATG")
    private Integer matg;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TENTACGIA")
    private String tentacgia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "DIACHI")
    private String diachi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SDT")
    private int sdt;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matg", fetch = FetchType.LAZY)
    private List<Sach> sachList;

    public Tacgia() {
    }

    public Tacgia(Integer matg) {
        this.matg = matg;
    }

    public Tacgia(Integer matg, String tentacgia, String diachi, int sdt, String email) {
        this.matg = matg;
        this.tentacgia = tentacgia;
        this.diachi = diachi;
        this.sdt = sdt;
        this.email = email;
    }

    public Integer getMatg() {
        return matg;
    }

    public void setMatg(Integer matg) {
        this.matg = matg;
    }

    public String getTentacgia() {
        return tentacgia;
    }

    public void setTentacgia(String tentacgia) {
        this.tentacgia = tentacgia;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public int getSdt() {
        return sdt;
    }

    public void setSdt(int sdt) {
        this.sdt = sdt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Sach> getSachList() {
        return sachList;
    }

    public void setSachList(List<Sach> sachList) {
        this.sachList = sachList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matg != null ? matg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tacgia)) {
            return false;
        }
        Tacgia other = (Tacgia) object;
        if ((this.matg == null && other.matg != null) || (this.matg != null && !this.matg.equals(other.matg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.library.entity.Tacgia[ matg=" + matg + " ]";
    }
    
}
