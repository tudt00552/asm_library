/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Setup
 */
@Entity
@Table(name = "NHAXB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nhaxb.findAll", query = "SELECT n FROM Nhaxb n")
    , @NamedQuery(name = "Nhaxb.findByManxb", query = "SELECT n FROM Nhaxb n WHERE n.manxb = :manxb")
    , @NamedQuery(name = "Nhaxb.findByTennxb", query = "SELECT n FROM Nhaxb n WHERE n.tennxb = :tennxb")
    , @NamedQuery(name = "Nhaxb.findByDiachi", query = "SELECT n FROM Nhaxb n WHERE n.diachi = :diachi")
    , @NamedQuery(name = "Nhaxb.findBySdt", query = "SELECT n FROM Nhaxb n WHERE n.sdt = :sdt")
    , @NamedQuery(name = "Nhaxb.findByEmail", query = "SELECT n FROM Nhaxb n WHERE n.email = :email")})
public class Nhaxb implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MANXB")
    private Integer manxb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TENNXB")
    private String tennxb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "DIACHI")
    private String diachi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SDT")
    private int sdt;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manxb", fetch = FetchType.LAZY)
    private List<Sach> sachList;

    public Nhaxb() {
    }

    public Nhaxb(Integer manxb) {
        this.manxb = manxb;
    }

    public Nhaxb(Integer manxb, String tennxb, String diachi, int sdt, String email) {
        this.manxb = manxb;
        this.tennxb = tennxb;
        this.diachi = diachi;
        this.sdt = sdt;
        this.email = email;
    }

    public Integer getManxb() {
        return manxb;
    }

    public void setManxb(Integer manxb) {
        this.manxb = manxb;
    }

    public String getTennxb() {
        return tennxb;
    }

    public void setTennxb(String tennxb) {
        this.tennxb = tennxb;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public int getSdt() {
        return sdt;
    }

    public void setSdt(int sdt) {
        this.sdt = sdt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Sach> getSachList() {
        return sachList;
    }

    public void setSachList(List<Sach> sachList) {
        this.sachList = sachList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (manxb != null ? manxb.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nhaxb)) {
            return false;
        }
        Nhaxb other = (Nhaxb) object;
        if ((this.manxb == null && other.manxb != null) || (this.manxb != null && !this.manxb.equals(other.manxb))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.library.entity.Nhaxb[ manxb=" + manxb + " ]";
    }
    
}
