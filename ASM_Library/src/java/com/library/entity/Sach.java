/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Setup
 */
@Entity
@Table(name = "SACH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sach.findAll", query = "SELECT s FROM Sach s")
    , @NamedQuery(name = "Sach.findByMasach", query = "SELECT s FROM Sach s WHERE s.masach = :masach")
    , @NamedQuery(name = "Sach.findByTensach", query = "SELECT s FROM Sach s WHERE s.tensach = :tensach")
    , @NamedQuery(name = "Sach.findByTomtat", query = "SELECT s FROM Sach s WHERE s.tomtat = :tomtat")})
public class Sach implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MASACH")
    private Integer masach;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "TENSACH")
    private String tensach;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "TOMTAT")
    private String tomtat;
    @JoinColumn(name = "MALOAI", referencedColumnName = "MALOAI")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Loaisach maloai;
    @JoinColumn(name = "MANXB", referencedColumnName = "MANXB")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Nhaxb manxb;
    @JoinColumn(name = "MATG", referencedColumnName = "MATG")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tacgia matg;

    public Sach() {
    }

    public Sach(Integer masach) {
        this.masach = masach;
    }

    public Sach(Integer masach, String tensach, String tomtat) {
        this.masach = masach;
        this.tensach = tensach;
        this.tomtat = tomtat;
    }

    public Integer getMasach() {
        return masach;
    }

    public void setMasach(Integer masach) {
        this.masach = masach;
    }

    public String getTensach() {
        return tensach;
    }

    public void setTensach(String tensach) {
        this.tensach = tensach;
    }

    public String getTomtat() {
        return tomtat;
    }

    public void setTomtat(String tomtat) {
        this.tomtat = tomtat;
    }

    public Loaisach getMaloai() {
        return maloai;
    }

    public void setMaloai(Loaisach maloai) {
        this.maloai = maloai;
    }

    public Nhaxb getManxb() {
        return manxb;
    }

    public void setManxb(Nhaxb manxb) {
        this.manxb = manxb;
    }

    public Tacgia getMatg() {
        return matg;
    }

    public void setMatg(Tacgia matg) {
        this.matg = matg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (masach != null ? masach.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sach)) {
            return false;
        }
        Sach other = (Sach) object;
        if ((this.masach == null && other.masach != null) || (this.masach != null && !this.masach.equals(other.masach))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.library.entity.Sach[ masach=" + masach + " ]";
    }
    
}
